import React from 'react';
import round3 from '../../images/yadi/material/crystal-sphere-1.png';
import r from '../../images/yadi/material/crystal-sphere-3.png';
import {Grid} from "material-ui";
import Vasha from '../../images/yadi/material/crystal-sphere-2.png';


const CenterRound = () => {
    return (
        <div className="main-container">
            <img src={round3} className="round-left"/>
            <img src={r} className="round-right"/>
            <img src={Vasha} className="round-center"/>
        </div>
    );
};
export default CenterRound;
