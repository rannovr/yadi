import React from 'react';
import Header from "../components/Header/Header";
import VozrasdaemTradicii from "../components/VozrasdaemTradicii/VozrasdaemTradicii";
import Center from "../components/Center/Center";
import DarimEmocii from "../components/DarimEmocii/DarimEmocii";
import Dostavljev from "../components/Dostavljev/Dostavljev";
import Footer from "../components/Footer/Footer";
import Niz from "../components/Niz/Niz";
import Job from "../components/Job/Job";
import Individual from "../components/Individual/Individual";
import Foto from "../components/Foto/Foto";
import CenterRound from "../components/CenterRound/CenterRound";
const Home = () => (
    <div className="main-content">
        <section className="section-content">
            <div className="container">
                <div className="container-pad">
                    <div className="container-pad-inner">
                        <Header/>
                    </div>
                </div>
            </div>
        </section>
        <section className="section-content">
            <div className="container">
                <div className="container-pad">
                    <div className="container-pad-inner">
                        <CenterRound/>
                    </div>
                </div>
            </div>
        </section>

    </div>
);
export default Home;
